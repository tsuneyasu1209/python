import os
def tree(path):
    for root, dirs, files in os.walk(path):
        print(root)
        for dir in dirs:
            print("\t", dir)

        for file in files:
            print("\t", file)

if __name__ == '__main__':
    tree('../Desktop/')