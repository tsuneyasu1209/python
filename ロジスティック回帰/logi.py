# 値が0か1の物を分類する
import pandas as pd

import statsmodels.api as s
from sklearn.linear_model import LogisticRegression  # ロジステック回帰
from sklearn.metrics import accuracy_score
# from sklearn.cross_validation import train_test_split
from sklearn.model_selection import train_test_split

#まずはデータセットを読み込み、内容を把握します
df = s.datasets.fair.load_pandas().data
df.head()
# 不倫してれば1、してなければ2
def have_affair(x):
    if x != 0:
        return 1
    else:
        return 0

# applyを使って、不倫経験有無を表す新しい列'affair_experience'を作りましょう。
df['affair_experience'] = df['affairs'].apply(have_affair)
df.head()

# 不倫の有無（affair_experience列）でグループ分けします。
df.groupby('affair_experience').mean()

#必要のない列の削除
df=df.drop(['occupation', 'occupation_husb', 'affairs'], axis=1)
#Xに特徴量を格納
X = df.ix[:, :6]
#Yに目的変数を格納
Y = df.ix[:, 6]

# モデル作成(初期化)
model = LogisticRegression()
# 値の学習
model.fit(X, Y)
# スコア測定
model.score(X, Y)

#分割 (トレーニングとテスト用にデータを分ける)して、モデルを作り、最後 に予測の精度を見ます
X_train, X_test, Y_train, Y_test = train_test_split(X, Y)
# モデルの作成(初期化)
model2 = LogisticRegression()
model2.fit(X_train, Y_train)
# 値の予測
class_predict = model2.predict(X_test)
# テストデータを使って、どれくらい正しいかどうかを計算
accuracy_score(Y_test, class_predict)