import smtplib,sys
import pandas as pd
import pandas as pd
import user
from email.mime.text import MIMEText
from email.utils import formatdate


# https://support.google.com/accounts/answer/6010255?hl=ja
# 1. 下記にアクセス
# https://accounts.google.com/b/0/DisplayUnlockCaptcha
# もしくは下記にて有効に設定
# https://myaccount.google.com/u/3/lesssecureapps?pageId=none

# 2. 実行
# メールを@pythonで送る
df = pd.read_csv("sample.csv")
address_li   = df.email
user_name_li = df.username
number       = user_name_li.size

FROM_ADDRESS = user.username
MY_PASSWORD  = user.userpassword
SUBJECT      = 'subject'

for i in range(number):
    def create_message(from_addr, to_addr, ubject, body):
        msg = MIMEText(body)
        msg['Subject'] = subject
        msg['From']    = from_addr
        msg['To']      = to_addr
        msg['Date']    = formatdate()
        return msg

    def send(from_addr, to_addrs, msg):
        smtpobj = smtplib.SMTP('smtp.gmail.com', 587)
        smtpobj.ehlo()
        smtpobj.starttls()
        smtpobj.ehlo()
        smtpobj.login(FROM_ADDRESS, MY_PASSWORD)
        smtpobj.sendmail(from_addr, to_addrs, msg.as_string())
        smtpobj.close()

    BODY = """{0}様
メールの文章です。
""".format(str(user_name_li[i]))
    subject = SUBJECT
    body    = BODY
    to_addr = address_li[i]
    msg = create_message(FROM_ADDRESS, to_addr, subject, body)
    send(FROM_ADDRESS, to_addr, msg)
    print(user_name_li[i])
    print(i)
    print("送信完了しました")