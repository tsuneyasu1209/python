# 条件分岐の繰り返しによって分類問題を解く教師あり学習の手法です｡
# 1. データを分割する基準を決定
# 2. 1で選択した基準に基づいてデータを分割
# 3. 設定した基準になるまで1と2を繰り返す

import numpy as np

# ジニ不純度を計算する関数
def gini(p):
    return 1-(p**2).sum()

# 決定木のクラス
class DecisionTree:

    def __init__(self, max_depth):
        # 分割する木の最大の数
        self.max_depth = max_depth 

    # 学習メソッド
    def fit(self,X,z):
        self.leaf = Node(X, y, self.max_depth)

    # 予測メソッド
    def predict(self,X):
        return self.leaf(X)

# ノードのクラス
class Node:

    def __init__(self,X, y,level):
        self.level = level 
        self.data_num = len(y)
        self.branch = []

        # 枝が純粋になった場合
        if len(np.unique(y)) == 1:
            self.y = y[0]
        elif level == 0:
            self.y = np.bincount(y).argmax()
        else:
            self.gn = 1
            # 特徴量を変えながら処理を繰り返す
            for j in range(X.shape[1]):
                x = X[:,j]
                xas = x.argsort()
                x_sort = x[xas]
                z_sort = y[xas]

                l = (x_sort[1:]+x_sort[:-1])/2
                l = l[z_sort[1:]!=z_sort[:-1]]

                for ikichi in l:
                    bunki = ikichi>x
                    z_hidari = y[bunki]
                    z_migi = y[~bunki]
                    n_hidari = float(len(z_hidari))
                    n_migi = float(len(z_migi))

                    gn = (gini(np.bincount(z_hidari)/n_hidari)*n_hidari+gini(np.bincount(z_migi)/n_migi)*n_migi)/self.data_num 
                    # 新しいジニ不純度が、現在のジニ不純度より低い場合、値を変える
                    if (self.gn>gn):
                        self.gn = gn
                        self.j = j
                        self.ikichi = ikichi

            o = (self.ikichi > X[:,self.j])
            # ノードを新規に2つ作成し、ノードの枝にする
            self.branch = [Node(X[o], y[o], level-1),Node(X[~o], y[~o], level-1)] 

    # predictメソッドが呼び出される際に、self.leaf(X)によって、下記の__call__()が呼び出されます。
    def __call__(self, X):
        if self.branch == []:
            return self.y
        else:
            o = self.ikichi>X[:,self.j]
            return np.where(o,self.branch[0](X),self.branch[1](X))