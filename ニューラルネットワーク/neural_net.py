import numpy as np
from layers import *
# 勾配計算が両方あるのは後で誤差逆伝播法の実装の正しさを確認できるようにするため
class NeuralNet:
    #レイヤーを保持するためのリストと重みパラメータの初期値を調整する値を持つ
    def __init__(self, weight_init_std = 0.01):
        #レイヤを保持するリスト
        self.layers = []
        #アフィンレイヤのみを保持するリスト
        self.affineLayers = []
        #最後のレイヤ(出力層)、今回はSoftmax
        self.lastLayer = SoftmaxWithLoss()
        #重みの初期値を決めるための変数
        self.weight_init_std = weight_init_std

    #アフィンレイヤを追加する
    def add_affine(self, input_size, output_size):
        w = np.random.randn(input_size, output_size) * self.weight_init_std
        b = np.zeros(output_size)
        layer = Affine(w, b)
        self.layers.append(layer)
        self.affineLayers.append(layer)

    #活性化関数を追加する
    def add_active(self, func_name):
        if func_name == "relu":
            self.layers.append(Relu())
        elif func_name == "sigmoid":
            self.layers.append(Sigmoid())

    #予測を行う（入力層から隠れ層までの処理をする）
    def predict(self, x):
        for layer in self.layers:
            x = layer.forward(x)

        return x

    #損失関数
    def loss(self, x, t):
        y = self.predict(x)

        return self.lastLayer.forward(y, t)

    #予測精度を求める
    def accuracy(self, x, t):
        y = self.predict(x)
        y = np.argmax(y, axis = 1)
        if t.ndim != 1:
            t = np.argmax(t, axis = 1)

        accuracy = np.sum(y == t) / x.shape[0]

        return accuracy

    #勾配を数値微分によって求める
    def numerical_gradient(self, x, t):
        loss_W = lambda W : self.loss(x, t)

        for layer in self.affineLayers:
            layer.dw = numerical_gradient(loss_W, layer.w)
            layer.db = numerical_gradient(loss_W, layer.b)

    #逆伝播による勾配計算を行う
    def gradient(self, x, t):
        self.loss(x, t)
        dout = 1
        dout = self.lastLayer.backward(dout)
        layers = list(self.layers)
        #逆伝播なので後ろのレイヤから勾配を伝播させる
        layers.reverse()
        for layer in layers:
            dout = layer.backward(dout)

    #学習を行う
    def fit(self, x, t, learning_rate = 0.1):
        self.gradient(x, t)

        for layer in self.affineLayers:
            layer.w -= learning_rate * layer.dw
            layer.b -= learning_rate * layer.db