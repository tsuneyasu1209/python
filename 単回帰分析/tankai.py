import numpy as np
import matplotlib.pyplot as plt

train_xy = np.loadtxt("download_cost.csv", delimiter=',', skiprows=1)
train_x = train_xy[:,0]
train_y = train_xy[:,1]

plt.plot(train_x, train_y, 'o')
plt.title("download per cost")
plt.xlabel("cost")
plt.ylabel("download")
plt.show()

# 未知のパラメーター初期値
theta0=np.random.rand()
theta1=np.random.rand()
# 単回帰式
def f(x):
    return theta0 + theta1 * x
# 評価関数（損失関数）
def E(x,y):
    return 0.5 * np.sum((y-f(x)) ** 2)
# 平均
mu_x = train_x.mean()
mu_y = train_y.mean()

# 標準偏差
sigma_x = train_x.std()
sigma_y = train_y.std()

# データの差が大きいので標準化
def standardize(a,mu,sigma):
    return (a - mu) / sigma

standardized_x = standardize(train_x,  mu_x, sigma_x)
standardized_y = standardize(train_y, mu_y, sigma_y)

plt.plot(standardized_x , standardized_y , 'o')
plt.xlabel("standardized cost")
plt.ylabel("standardized download")
plt.show()

ETA = 0.001 #学習率
diff = 1 #diffの初期値
count=0 #更新回数

## 損失関数の値
error = E(standardized_x , standardized_y)

## theta0,theta1の良い値が見つかるまで更新を繰り返す
while abs(diff)>0.0001:
    #theta0,theta1の更新式
    new_theta0 = theta0 - ETA*np.sum((f(standardized_x)-standardized_y))
    new_theta1 = theta1 - ETA*np.sum((f(standardized_x)-standardized_y) * standardized_x)

    #パラメータを更新する
    theta0 = new_theta0
    theta1 = new_theta1

    #新しい損失関数の値
    current_error = E(standardized_x, standardized_y)
    #（前回の更新時の損失関数の値）ー（今回の更新時の損失関数）
    diff = error - current_error
    #損失関数の更新
    error = current_error

    #回数の更新
    count+=1

    #出力
    print(str(count) + "回目のtheta0: " + str(theta0) + ", theta1: " + str(theta1) + ", diff: " + str(diff))
    
x = np.linspace(-2,2,100)

plt.plot(standardized_x , standardized_y, 'o')
plt.plot(x, f(x))
plt.show()