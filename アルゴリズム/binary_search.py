def binary_search(li, item):
    low = 0
    high = len(li) - 1 # もしくはlen(x)

    # 探索の下限のlowが上限のhighになるまで探索します。
    while low <= high:
        mid = (low + high) // 2 # 中央値
        guess = li[mid]
        if guess == item:
            return mid
        if guess > item:
            high = mid - 1
        else:
            low = mid + 1
    return None

li = [i for i in range(1,101)]
print(binary_search(li, 3))
print(binary_search(li, -1))

event_list = [23,36,12,18,21]

sorted_list = sorted(event_list)
print(sorted_list)