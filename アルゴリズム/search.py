"""
線形探索の関数
li: リストのデータ
items: 検索したいデータ
"""
def linear_search(li, items):
    print(str(items) + "を検索します")
    for i,data in enumerate(li):
        if data == int(items):
            print(str(i) + "番目にありました")
            break
    else:
        print("入力した要素は存在しませんでした")

if __name__ == '__main__':
    li = [1,4,5,6,8,9,14,58]
    print("調べるリスト")
    print(li)
    search_num = input("上のリストから調べたい要素を入力してください")
    linear_search(li, search_num)