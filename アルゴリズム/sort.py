# バブルソート
def bubbleSort(l):
    for index in range(len(l)-1, 0, -1):
        for low in range(index):
            if l[low] > l[low+1]:
                tmp = l[low+1]
                l[low+1] = l[low]
                l[low] = tmp
    return l


if __name__ == '__main__':
    from random import shuffle
    l = list(range(15))
    lcopy = l[:]
    shuffle(l)
    print(bubbleSort(l))

# 挿入ソート


def insert_sort(data):
    for i in range(1, len(data)):
        select = data[i]
        if data[i-1] > select:
            j = i
            data[j] = data[j-1]
            j -= 1
            while j > 0 and data[j-1] > select:
                data[j] = data[j-1]
                j -= 1
            data[j] = select


data = [4, 5, 195, 252, 51, 2, 94, 29, 75, 99]
insert_sort(data)
print(data)

# マージソート


def merge_sort(data):
    def _merge(data1, data2):
        result = []
        item1 = data1[0]
        item2 = data2[0]
        while(True):
            if item1 < item2:
                result.append(data1.pop(0))
                if len(data1) == 0:
                    result.extend(data2)
                    break
                item1 = data1[0]
            else:
                result.append(data2.pop(0))
                if len(data2) == 0:
                    result.extend(data1)
                    break
                item2 = data2[0]
        return result

    def _split(data):
        length = len(data)
        data1 = data[0:length//2]
        data2 = data[length//2:length]
        return data1, data2

    def _merge_sort(data):
        if len(data) <= 1:
            return data
        data1, data2 = _split(data)
        return _merge(_merge_sort(data1), _merge_sort(data2))

    return _merge_sort(data)


data = [6, 55, 65, 45, 24, 82, 99, 18]

data = merge_sort(data)

# クイックソート （Quick Sort ）
def quickSort(seq):
    if len(seq) < 1:
        return seq
    pivot = seq[0]
    left = []
    right= []
    for x in range(1, len(seq)):
        if seq[x] <= pivot:
            left.append(seq[x])
        else:
            right.append(seq[x])
    left = quickSort(left)
    right = quickSort(right)
    foo = [pivot]
    return left + foo + right

l = [10, 4,2, 4,11,2,1]
print(l)
print(quickSort(l))

# ヒープソート
def left(i):
    return i*2+1

def right(i):
    return i*2+2

def parent(i):
    return (i-1)//2

def heapify(data):
    last = (len(data)-1)//2
    for i in range(last,0,-1):
        while True:
            if left(i) < len(data) and data[i] > data[left(i)]:
                data[i], data[left(i)] = data[left(i)], data[i]
            if right(i) < len(data) and data[i] > data[right(i)]:
                data[i], data[right(i)] = data[right(i)], data[i]
            if i == 0:
                break
            i = parent(i)
    return data

def heap_sort(data):
    result = []
    data = heapify(data)
    for i in range(len(data)):
        result.append(data.pop(0))
        data = heapify(data)
    return result

data = [80,  14, 59, 58, 55, 11, 58, 99, 21, 22, 81]

data = heap_sort(data)
print(data)