stack = [1,2,3]
stack.append(4)
print(stack) # [1,2,3,4]

print(stack.pop()) # 4
print(stack) #  [1,2,3]

from collections import deque
que = deque(["先頭さん"])
que.append("2番目さん")
que = que.popleft()
print(que) # '先頭さん'