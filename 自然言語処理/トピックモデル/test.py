## 単語のカウントを行うときに使います。
from collections import Counter

## scipyのスパース化を行います。
import scipy.sparse as sparse

## sklearnに付属されたLDAを使います。
from sklearn.decomposition import LatentDirichletAllocation

## データの読み込みに使います。
import json
import numpy as np
with open("ml_mysql_nouns.json", "r",encoding='utf-8') as f:
    doc_data = json.load(f)
    
## キーの内容がall_doc_indexに入る
all_doc_index = doc_data.keys()

## 単語は何があるかを確認する
## ここで、データを一旦一つにまとめる
dupulicating_all_vocab = []
for doc_idx in all_doc_index:
    dupulicating_all_vocab += doc_data[doc_idx]

## 重複を弾く
all_vocab = list(set(dupulicating_all_vocab))

# print(len(dupulicating_all_vocab))
## 重複を弾くと676になっている！
# print(len(all_vocab))

## trainingデータとtestデータに分けておく
## 文書のインデックス番号を入れておく
all_doc_index_ar = np.array(list(all_doc_index))

## trainingデータには全体の100%を入れる(これを0.9にすると、全体の90%という指定ができる)
train_portion = 1
train_num = int(len(all_doc_index) * train_portion)

## trainingデータのindexと、testデータのインデックス。
## データが多い場合、shuffleなどを使って、トレーニングデータと、テストデータのインデックスを割り振ると良い
train_doc_index = all_doc_index_ar[:train_num]
test_doc_index = all_doc_index_ar[train_num:]

## 取り敢えず、空の疎行列を作り、あとでここにデータを入れていく。
## (train_doc_indexの長さ) * (all_vocabの長さ) = 8 * 676
lil_train = sparse.lil_matrix((len(train_doc_index), len(all_vocab)),dtype = np.int)
lil_test = sparse.lil_matrix((len(test_doc_index), len(all_vocab)),dtype = np.int)

## 入力データ作成、ここだけ少し取っ付きづらいので覚悟が必要
## 要は文章に含まれる単語の数をカウントするだけ。

## train_total_elements_numは単語が何個あるかをカウントしている
train_total_elements_num = 0

all_vocab_arr = np.array(all_vocab)

for i in range(len(train_doc_index)):
        doc_idx = train_doc_index[i]
        ## doc_dataは一番最初に読み込んだもの
        ## 単語をキーとしてそれが、いくつあるかが値として、辞書型でrow_dataに入る
        ## Counterは便利な関数 → Counter(doc_data["18"])を実行してみてください。
        row_data = Counter(doc_data[doc_idx])
        # all_vocab_arr  = np.array(list(row_data))

        ## row_dataにキーとして入っている文字がwordに入る
        for word in row_data.keys():
            print(i, word)
            ## word_idxは単語が何番目のidかを返す
            ## np.whereがきになる方は → np.where(all_vocab_ar == "ここ")
            word_idx = np.where(all_vocab_arr == word)[0][0]
            ## lil_trainには、row_dataの値が入る
            lil_train[i, word_idx] = row_data[word]
            train_total_elements_num += 1

## 何個のデータを入れたか
# print(train_total_elements_num)
## 入力データをみてみると、疎行列であることが分かる
# print(lil_train)


# ここで注意が必要なのは、n_topicsです。
# ldaは教師なし学習なので、文章のトピック数はこちら側で指定をする必要があります。
# 今回はmysqlか機械学習の二つなので、n_topicsは2と設定しましょう。
# このn_topicsは、たくさんのカテゴリがあると推定した場合は、それに合わせて大きくする必要があります。
model = LatentDirichletAllocation(random_state=123)
# model = LatentDirichletAllocation(n_topics = 2, random_state=123)
model.fit(lil_train)

normalize_components = model.components_ / model.components_.sum(axis=0)
normalize_components.shape

## 出力結果として、どの様なデータがあるか見てみる
n_top_words = 25
for topic_idx, topic in enumerate(normalize_components):
    print(topic_idx)
    print(", ".join([all_vocab_arr[i] for i in topic.argsort()[:-n_top_words - 1:-1]]))
    print()
    
doc_topic_data = model.transform(lil_train)
doc_topic_data

normalize_doc_topic_data = doc_topic_data / doc_topic_data.sum(axis=1, keepdims = True)
for k in range(len(train_doc_index)):
        point = []
        for topic_idx, prob in enumerate(normalize_doc_topic_data[k]):
            point.append(prob)
        print(train_doc_index[k], point)