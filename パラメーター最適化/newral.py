from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier # 多層パーセプトロン

# データの読み込み
iris = load_iris()
X = iris.data
y = iris.target
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)

# 勾配降下法の最適化アルゴリズムをSGD(確率的勾配降下法)に設定
clf = MLPClassifier(solver="sgd",random_state=0,max_iter=10000)
clf.fit(X_train, y_train)
print("SGD", clf.score(X_test, y_test))

# 勾配降下法の最適化アルゴリズムをAdamに設定
clf = MLPClassifier(solver="adam",random_state=0,max_iter=10000)
clf.fit(X_train, y_train)
print("Adam:", clf.score(X_test, y_test))