from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

# 手書き数字のデータセット
data = load_digits()

x = data.data
y = data.target

X_train, X_test, y_train, y_test = train_test_split(x, y,  random_state=2)


model1 = SVC()
model1.fit(X_train, y_train)
pred = model1.predict(X_test)
print("パラメータチューニング前：", accuracy_score(y_test, pred)) # パラメータチューニング前： 0.4288888888888889


param_grid = {"C": [0.001, 0.01, 0.1, 1, 10, 100]}

grid_search = GridSearchCV(model1, param_grid, cv=5)
grid_search.fit(X_train, y_train)

print(grid_search.best_params_ ) 
print(grid_search.best_estimator_)

best_model = LogisticRegression(C=10)
best_model.fit(X_train, y_train)
pred = best_model.predict(X_test)
print("パラメータチューニング後：", accuracy_score(y_test, pred)) 