from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.datasets import load_breast_cancer
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

# 乳がんのデータセット
data = load_breast_cancer()

x = data.data
y = data.target

X_train, X_test, y_train, y_test = train_test_split(x, y,  random_state=100)


model1 = LogisticRegression() # Cのデフォルトは1.0
model1.fit(X_train, y_train)
pred = model1.predict(X_test)
print("パラメータチューニング前：", accuracy_score(y_test, pred)) # パラメータチューニング前： 0.9870629370629371

# print(model1.intercept_) # 切片
# print(model1.coef_)　     # 重み

param_grid = {"C": [0.001, 0.01, 0.1, 1, 10, 100], "solver" :  ["newton-cg", "lbfgs", "liblinear", "sag", "saga"], "warm_start": [True, False]}

grid_search = GridSearchCV(model1, param_grid, cv=5)
grid_search.fit(X_train, y_train)

# 最もよかったパラメータの組み合わせを取得
print(grid_search.best_params_ ) 

"""
{'C': 100, 'solver': 'newton-cg', 'warm_start': True}
"""

print(grid_search.best_estimator_)
"""
LogisticRegression(C=100, class_weight=None, dual=False, fit_intercept=True,
          intercept_scaling=1, max_iter=100, multi_class='warn',
          n_jobs=None, penalty='l2', random_state=None, solver='newton-cg',
          tol=0.0001, verbose=0, warm_start=True)
"""

best_model = LogisticRegression(C=100, solver="newton-cg", warm_start= True)
best_model.fit(X_train, y_train)
pred = best_model.predict(X_test)
print("パラメータチューニング後：", accuracy_score(y_test, pred)) # パラメータチューニング後： 0.965034965034965