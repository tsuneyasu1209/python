from sklearn.datasets import load_iris
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import pandas as pd
import matplotlib.pyplot as plt
%matplotlib inline

iris = load_iris()
x = iris.data
y = iris.target

df = pd.DataFrame(iris.data, columns=iris.feature_names)

feature_name = iris.feature_names

# トレーニングデータをテストデータに分解
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.20, random_state=5)

# 学習
model = RandomForestClassifier(n_estimators=20, random_state=42)
model.fit(x_train, y_train)

# 予測データ作成
pred = model.predict(x_train)

# 特徴量重要を抽出
feature_importance = model.feature_importances_

# 可視化
feature_importance = pd.Series(feature_importance,index=feature_name)
values = feature_importance.sort_values()
values.plot(kind='barh', figsize=(12, 5))