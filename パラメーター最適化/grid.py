from sklearn import datasets
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import GridSearchCV

# アイリスデータセットを読み込む
dataset = datasets.load_iris()

X = dataset.data
y = dataset.target

model = DecisionTreeClassifier()

# 試行するパラメータの羅列
params = {
    'max_depth': list(range(1, 20)),
    'criterion': ['gini', 'entropy'],
}

# cv=10は10分割の交差検証を実行
grid_search = GridSearchCV(model, param_grid=params, cv=10)

grid_search.fit(X, y)

print(grid_search.best_score_)     # 最も良かったスコアを出力
print(grid_search.best_estimator_) # 最適なモデルを出力
print(grid_search.best_params_) 