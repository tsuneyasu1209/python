from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.datasets import load_breast_cancer
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

# 乳がんのデータセット
data = load_breast_cancer()

x = data.data
y = data.target

X_train, X_test, y_train, y_test = train_test_split(x, y,  random_state=2)


model1 =RandomForestClassifier(max_depth=3) 
model1.fit(X_train, y_train)
pred = model1.predict(X_test)
print("パラメータチューニング前：", accuracy_score(y_test, pred))

param_grid = {"max_depth": [0.001, 0.01, 0.1, 1, 10, 100], "criterion": ['gini','entropy']}

grid_search = GridSearchCV(model1, param_grid, cv=5)
grid_search.fit(X_train, y_train)

# 最もよかったパラメータの組み合わせを取得
print(grid_search.best_params_ ) 
"""
{'criterion': 'entropy', 'max_depth': 100}
"""

print(grid_search.best_estimator_)
"""""
出力結果
パラメータチューニング前： 0.916083916083916
{'criterion': 'entropy', 'max_depth': 100}
RandomForestClassifier(bootstrap=True, class_weight=None, criterion='entropy',
                       max_depth=100, max_features='auto', max_leaf_nodes=None,
                       min_impurity_decrease=0.0, min_impurity_split=None,
                       min_samples_leaf=1, min_samples_split=2,
                       min_weight_fraction_leaf=0.0, n_estimators=10,
                       n_jobs=None, oob_score=False, random_state=None,
                       verbose=0, warm_start=False)
"""""

model2 =RandomForestClassifier(max_depth=100,criterion = "entropy") 
model2.fit(X_train, y_train)
pred = model2.predict(X_test)
print("パラメータチューニング後：", accuracy_score(y_test, pred))

"""""
出力結果
パラメータチューニング後： 0.9440559440559441
"""""