# 未知のパラメーター初期値
theta0=np.random.rand()
theta1=np.random.rand()
# 単回帰式
def f(x):
    return theta0 + theta1 * x
# 評価関数（損失関数）
def E(x,y):
    return 0.5 * np.sum((y-f(x)) ** 2)

# データの差が大きいので標準化
def standardize(a,mu,sigma):
    return (a - mu) / sigma

def sigmoid(z):
    return 1.0 / (1.0 + np.exp(-z))

# 0.1の間隔で-10〜10未満のデータを生成
z = np.arange(-10, 10, 0.1)


# ジニ不純度を計算する関数
def gini(p):
    return 1-(p**2).sum()
# シグモイド関数
# ０と１の中間も表現できることが特徴
def sigmoid(x):
    return 1 / (1 + np.exp(-x))

# ReLU関数
# 0以上の値だけをとり、それ以外の値は全てゼロにするという区分線形関数
def relu(x):
    # np.maximum()で引数のうち最大値を出力
    return np.maximum(0, x)
    #return np.where(x <= 0, 0, x)でも可

# Softmax関数
# 出力の合計が1になり個々の出力がその割合となるように調整
# 出力値を確率として解釈できる(0～１の値で正規化できる)ことから分類問題でよく使われます。

def softmax(x):
    #入力が行列だった場合
    if x.ndim == 2:
        # 列を揃えるために反転
        x = x.T
        x = x - np.max(x, axis=0)
        y = np.exp(x) / np.sum(np.exp(x), axis=0)
        return y.T

    x = x - np.max(x) # オーバーフロー対策
    return np.exp(x) / np.sum(np.exp(x))

# 恒等関数（出力関数）
# 力をそのまま出力
# 出力の範囲に制限がなく連続的、連続的な数値を予測する回帰問題によく使
def identity_function(x):
    return x

# 損失関数、交差エントロピー誤差
# Softmax関数に合わせて設計、微分の値が出力値と正解値との差、分類問題でよく使われます
# ｙ出力、ｔ正解値
def cross_entropy_error(y, t):
    return -np.sum(t * np.log(y+1e-7))

# ミニバッチ対応した交差エントロピー
# ｙ出力、ｔ正解値
def cross_entropy_error(y, t):
    if y.ndim == 1:
        t = t.reshpae(1, t.size)
        y = y.reshape(1, y.size)

    batch_size = y.shape[0]
    return -np.sum(t * np.log(y + 1e-7)) / batch_size
# 正解データがラベル(「1」,「3」など)として渡されたときは次のように実装
def cross_entropy_error(y, t):
    if y.ndim == 1:
        t = t.reshpae(1, t.size)
        y = y.reshape(1, y.size)

    batch_size = y.shape[0]
    return -np.sum(np.log(y[np.arange(batch_size), t])) / batch_size

# 損失関数、二乗和誤
# 恒等関数に合わせて設計
# ｙ出力、ｔ正解値
def mean_squared_error(y, t):
    return 0.5 * np.sum((y - t) ** 2)


# 最適化
# 確率的勾配降下法

def numerical_gradient(f, x):
    h = 10e-4
    return (f(x + h) - f(x - h)) / (2 * h)